FROM debian:buster as builder

RUN apt update && apt install -y build-essential libusb-dev gcc libcurl4-openssl-dev
ADD src /src
WORKDIR /src
RUN ./build

FROM redis:5-buster

COPY --from=builder /src/fingerprint_compare /app/fingerprint_compare
COPY --from=builder /src/Shared /app/Shared
COPY --from=builder /src/Shared/*so* /usr/lib/
RUN apt update && apt install -y libusb-dev libcurl4-openssl-dev
ADD entrypoint.sh /scripts/entrypoint.sh
ADD wait-for-redis.sh /scripts/wait-for-redis.sh
RUN chmod +x /scripts/wait-for-redis.sh
RUN chmod +x /scripts/entrypoint.sh
RUN chmod +x /app/fingerprint_compare
WORKDIR /app
CMD ["/scripts/entrypoint.sh"]