#!/bin/bash

redis-server --replicaof $REDIS_MASTER_HOST $REDIS_MASTER_PORT > /app/redis.log &
/scripts/wait-for-redis.sh localhost:6379
./fingerprint_compare