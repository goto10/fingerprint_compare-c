// #include <sys/socket.h> /* socket, connect */
// #include <netinet/in.h> /* struct sockaddr_in, struct servaddr */
// #include <netdb.h> /* struct hostent, gethostbyname */
// #include "base64.c"
// #include "base64.h"

// Next: Server Example, Previous: Transferring Data, Up: Connections   [Contents][Index]

// 16.9.6 Byte Stream Socket Example
// Here is an example client program that makes a connection for a byte stream socket in the Internet namespace. It doesn’t do anything particularly interesting once it has connected to the server; it just sends a text string to the server and exits.

// This program uses init_sockaddr to set up the socket address; see Inet Example.


#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <curl/curl.h>
#include <string.h>

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *fp)
{
   strcat(fp,ptr);
   printf("%d",size);
   return size;
}

void makerequest (char device_serial[],char* gym_id[])
{
  printf("Started make request\n");
  printf("Device Serial: %s\n",device_serial);
  char api_url[256];
  sprintf(api_url,"%s%s","http://data-ms.get-classes.personal-trainer.dev.internallab.co.uk/api/getGymByDevice/",device_serial);
  CURL *hnd = curl_easy_init();
  curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, "GET");
  curl_easy_setopt(hnd, CURLOPT_URL, api_url);
  curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, write_data);
  curl_easy_setopt(hnd, CURLOPT_WRITEDATA, gym_id);
  CURLcode ret = curl_easy_perform(hnd);
  return;
}
