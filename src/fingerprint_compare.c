#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "ftrapi.h"
#include "civetweb.c"
#include "civetweb.h"
#include "hiredis.h"
#include <curl/curl.h>
#include "makerequest.h"


redisContext *c_read;
UDGT32  FPTemplateSize = 0;
FTRAPI_RESULT _Res = 0;
#define CHECK_RESULT_EXIT(x) if( ( _Res = x ) != FTR_RETCODE_OK ) { printf( "%d",_Res ); FreeResource(); return -1; }
#define CHECK_RESULT_SHOW(x) if( ( _Res = x ) != FTR_RETCODE_OK ) { printf( "%d",_Res ); return; }
#define MAX_KEY_TEMPLATE_SIZE  (15)
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif
DGTBOOL StopAnyOperation = FALSE;
int redis_port = 6379;

char *GetErrorMessage(FTRAPI_RESULT Result);
void ShowErrorByNumber(FTRAPI_RESULT Result);
void FreeResource();
int ReadTemplateFromFile(char *Name, FTR_DATA_PTR pTemplate);
void IdentifyThread(FTR_DATA fingerprint, char *gym_id[], char *user_id[]);

void read_template_from_redis(char key_name[], FTR_DATA_PTR pTemplate);


static int begin_request_handler(struct mg_connection *conn)
{
	const struct mg_request_info *ri = mg_get_request_info(conn);
	char post_data[ri->content_length];
	int post_data_len;
	printf("Route: %s hit\n",ri->local_uri);
	if (!strcmp(ri->local_uri, "/api/verifyFingerprint"))
	{
		printf("Hit the verify api\n");
		post_data_len = mg_read(conn, post_data, sizeof(post_data));
		FTR_DATA fingerprint;
		fingerprint.pData = post_data;
		fingerprint.dwSize = sizeof(post_data);
		char device_serial[50];
		mg_get_var(ri->query_string,50 ,"serial", device_serial,50);
		printf("QueryParam: %s\n",device_serial);
		char* user_id[8];
		char* gym_id[9];
		makerequest(device_serial,gym_id);
		gym_id[8] = '\0';
		printf("Gym ID: %s\n",gym_id);
		printf("Gym ID Length: %d\n",strlen(gym_id));
		IdentifyThread(fingerprint,gym_id,user_id);

		printf("Matched User: %s\n", user_id);

		mg_printf(conn, "HTTP/1.0 200 OK\r\n"
			"Content-Type: text/plain\r\n\r\n"
			"0",
			post_data_len, user_id, post_data_len);
	}

	return 1;	// Mark request as processed
}

int main(void)
{

	// Connect to local read only redis replica
	c_read = redisConnect("localhost", redis_port);
	if (c_read != NULL && c_read->err)
	{
		printf("Error: %s\n", c_read->errstr);
		// handle error
	}
	else
	{
		printf("Connected to Local Read Only Redis Replica\n");
	}

	CHECK_RESULT_EXIT(FTRInitialize());
	CHECK_RESULT_EXIT(FTRGetParam(FTR_PARAM_MAX_TEMPLATE_SIZE, (FTR_PARAM_VALUE) &FPTemplateSize));

	struct mg_context * ctx;
	const char *options[] = { "listening_ports", "8080", NULL
	};
	struct mg_callbacks callbacks;

	memset(&callbacks, 0, sizeof(callbacks));
	callbacks.begin_request = begin_request_handler;
	ctx = mg_start(&callbacks, NULL, options);
	printf("Server Started...\n\n");
	getchar();	// Wait until user hits "enter"
	mg_stop(ctx);
	// Clear redis memory
							redisFree(c_read);

	return 0;
}

void IdentifyThread(FTR_DATA fingerprint, char *gym_id[], char *user_id[])
{

	

	FTRAPI_RESULT Res;
	FTR_IDENTIFY_ARRAY fromDB;
	FTR_IDENTIFY_RECORD idRec;
	FTR_MATCHED_X_ARRAY resArrX;
	FTR_MATCHED_X_RECORD resRecX;
	UDGT32 resNum = 0;
	DGTBOOL ErrorSet = FALSE;
	FTR_DATA BaseTemplate;
	DGTVOID * pBaseTemplate;
	FTR_DATA TestTemplate;

	Res = FTRSetBaseTemplate(&fingerprint);
	printf("Set Base Template \n");
	if (Res == FTR_RETCODE_OK)
	{
		fromDB.TotalNumber = 1;
		fromDB.pMembers = &idRec;
		resArrX.TotalNumber = 1;
		resArrX.pMembers = &resRecX;

		if (!ErrorSet)
		{
			DGTBOOL Find = FALSE;

			DGTBOOL bIsNotEnoughOverlap = FALSE;
			int valNotEnoughOverlap;
			FTRAPI_RESULT getRes;

			printf("This is the gym id : %s\n",gym_id);
			redisReply * keys_reply = redisCommand(c_read, "KEYS %s_*",gym_id);
			if (keys_reply)
				printf("Retrieved keys\n");
				fflush(stdout);

			{
				printf("Elements Size: %ld\n", keys_reply->elements);
				printf("Elements : %d\n", keys_reply->type);

				for (int i = 0; i < keys_reply->elements; i++)
				{
            printf(" Redis Reply: %s\n",keys_reply->element[i]->str);
						char key_name[17];
						strcpy(key_name,keys_reply->element[i]->str);
						char separator = '_';
						char * sep_at = strchr(key_name,separator );
						if(sep_at != NULL)
						{
							*sep_at = '\0'; 
							strcpy(user_id,sep_at + 1);
						}


						printf("User id after copy: %s\n",user_id);
						memset(&idRec, 0, sizeof(idRec));
						memcpy(idRec.KeyValue, user_id, 8);
						printf("KeyValue: %s\n", idRec.KeyValue);

            read_template_from_redis(keys_reply->element[i]->str,&TestTemplate);
						printf("Read temlate\n");
            idRec.pData = &TestTemplate;
						

						printf("Template size: %d\n", TestTemplate.dwSize);
						Res = FTRIdentifyN(&fromDB, &resNum, &resArrX);

						if (Res != FTR_RETCODE_OK)
						{
							printf("FTR Retcode: %d\n",Res);
							if (TestTemplate.pData)
							{
								//free(TestTemplate.pData);
								//TestTemplate.pData = NULL;
								sprintf(user_id,"ERROR");
								break;
							}

							ErrorSet = TRUE;
						}

						if (resNum)
						{
							printf("You are %s (%d)\n", resArrX.pMembers[0].KeyValue, (int) resArrX.pMembers[0].FarAttained.N);
							Find = TRUE;
							return;
						}
						else
						{
							printf("No Match Found\n");
							sprintf(user_id,"NULL");

						}

						if (StopAnyOperation)
						{
							ErrorSet = TRUE;
							break;
						}

						valNotEnoughOverlap = 0;
						getRes = FTRGetParam(FTR_PARAM_OVERLAP_NOT_ENOUGH, (FTR_PARAM_VALUE*) &valNotEnoughOverlap);
						bIsNotEnoughOverlap |= ((getRes == FTR_RETCODE_OK) && (valNotEnoughOverlap != 0));
					}
			}
			if (!ErrorSet && !Find)
			{
				if (bIsNotEnoughOverlap)
				{
				}
				else
				{
				}

			}
					freeReplyObject(keys_reply);


		}
	}
	
}

void FreeResource()
{
	FTRTerminate();
}

void read_template_from_redis(char key_name[], FTR_DATA_PTR pTemplate){

	printf("Reading key: %s\n",key_name);
  redisReply *reply_data = redisCommand(c_read, "GET %s", key_name);
  if (reply_data){
    pTemplate->pData = malloc( reply_data->len );
    pTemplate->dwSize = reply_data->len;
		pTemplate->pData = reply_data->str;
    //memcpy(pTemplate->pData,reply_data->str,reply_data->len);
    freeReplyObject(reply_data);
    return;
  }
}