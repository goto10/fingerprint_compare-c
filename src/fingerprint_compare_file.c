#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "ftrapi.h"
#include "civetweb.c"
#include "civetweb.h"
#include "hiredis.h"

redisContext *c_read;
redisContext *c_write;
UDGT32  FPTemplateSize = 0;
FTRAPI_RESULT _Res = 0;
char TemplatesDirName[] = "./templates/12345/";
#define CHECK_RESULT_EXIT(x) if( ( _Res = x ) != FTR_RETCODE_OK ) { printf( "%d",_Res ); FreeResource(); return -1; }
#define CHECK_RESULT_SHOW(x) if( ( _Res = x ) != FTR_RETCODE_OK ) { printf( "%d",_Res ); return; }
#define MAX_KEY_TEMPLATE_SIZE  (15)
#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif
DGTBOOL StopAnyOperation = FALSE;
int redis_port = 6379;

char* GetErrorMessage( FTRAPI_RESULT Result );
void ShowErrorByNumber( FTRAPI_RESULT Result );
void FreeResource();
int ReadTemplateFromFile( char* Name, FTR_DATA_PTR pTemplate );
char IdentifyThread( FTR_DATA fingerprint );

static int begin_request_handler(struct mg_connection *conn)
{
    const struct mg_request_info *ri = mg_get_request_info(conn);
    char post_data[ri->content_length];
    int post_data_len;
    
    if (!strcmp(ri->local_uri, "/api/verifyFingerprint")) {
      post_data_len = mg_read(conn, post_data, sizeof(post_data));

      FTR_DATA fingerprint;
      fingerprint.pData = post_data;
      fingerprint.dwSize = sizeof(post_data);
      char matched_user = IdentifyThread(fingerprint);

      printf("Matched User: %s\n", matched_user);

      // Send reply to the client, showing submitted form values.
        mg_printf(conn, "HTTP/1.0 200 OK\r\n"
                  "Content-Type: text/plain\r\n\r\n"
                  "0 or 1",
                  post_data_len, matched_user, post_data_len);
    }

    return 1;  // Mark request as processed
}

int main(void)
{

    // Connect to local read only redis replica
    c_read= redisConnect("localhost", redis_port);
    if (c_read != NULL && c_read->err) {
        printf("Error: %s\n", c_read->errstr);
        // handle error
    } else {
        printf("Connected to Local Read Only Redis Replica\n");
    }

    CHECK_RESULT_EXIT( FTRInitialize() );
    CHECK_RESULT_EXIT( FTRGetParam( FTR_PARAM_MAX_TEMPLATE_SIZE, (FTR_PARAM_VALUE)&FPTemplateSize ) );

    struct mg_context *ctx;
    const char *options[] = {"listening_ports", "8080", NULL};
    struct mg_callbacks callbacks;

    memset(&callbacks, 0, sizeof(callbacks));
    callbacks.begin_request = begin_request_handler;
    ctx = mg_start(&callbacks, NULL, options);
    printf("Server Started...\n\n");
    getchar();  // Wait until user hits "enter"
    mg_stop(ctx);
    // Clear redis memory
    redisFree(c_read);
    return 0;
}

char IdentifyThread( FTR_DATA fingerprint )
{
  FTRAPI_RESULT        Res;
  FTR_IDENTIFY_ARRAY   fromDB;
  FTR_IDENTIFY_RECORD  idRec;
  FTR_MATCHED_X_ARRAY  resArrX;
  FTR_MATCHED_X_RECORD resRecX;
  UDGT32               resNum = 0;
  char                 Msg[FILENAME_MAX];
  char                 FullTemplateName[FILENAME_MAX];
  DGTBOOL             ErrorSet = FALSE;
  FTR_DATA          BaseTemplate;
  DGTVOID*          pBaseTemplate;
  FTR_DATA          TestTemplate;
  memset( Msg, 0, sizeof( Msg ) );
        
        
  Res = FTRSetBaseTemplate( &fingerprint );
  
  if( Res == FTR_RETCODE_OK )
  {
      fromDB.TotalNumber   = 1;
      fromDB.pMembers      = &idRec;
      resArrX.TotalNumber   = 1;
      resArrX.pMembers      = &resRecX;

      DIR *pTemplateDir = opendir( TemplatesDirName );

      if( !pTemplateDir )
      {
          strcat( Msg, "Error when open tempaltes directory" );
          ErrorSet = TRUE;
      }

      if( !ErrorSet )
      {
          struct dirent *pNextEntry; ;
          DGTBOOL    Find = FALSE;

          DGTBOOL bIsNotEnoughOverlap = FALSE;
          int   valNotEnoughOverlap;
          FTRAPI_RESULT getRes;

          while( ( pNextEntry = readdir( pTemplateDir ) ) != NULL )
          {
            if (!strcmp (pNextEntry->d_name, ".")){
              continue;
            }
            if (!strcmp (pNextEntry->d_name, "..")){  
                continue;
            }
            printf("File\n");
              char* point_pos = strchr( pNextEntry->d_name, '.' );

              if(point_pos)
              {
                  memset( FullTemplateName, 0, sizeof( FullTemplateName ) );
                  strcat( FullTemplateName, TemplatesDirName );
                  strcat( FullTemplateName, pNextEntry->d_name );

                  memset( &idRec, 0, sizeof( idRec ) );
                  memcpy(idRec.KeyValue, pNextEntry->d_name, min( MAX_KEY_TEMPLATE_SIZE, point_pos - pNextEntry->d_name ) );
                  
                  if( !ReadTemplateFromFile( FullTemplateName, &TestTemplate ) )
                  {
                      strcat( Msg, "Error when read template" );
                      ErrorSet = TRUE;
                      break;
                  }

                  idRec.pData = &TestTemplate;

                  Res = FTRIdentifyN( &fromDB, &resNum, &resArrX );

                  if( Res != FTR_RETCODE_OK )
                  {
                      if( TestTemplate.pData )
                      {
                          free( TestTemplate.pData );
                          break;
                      }

                      ErrorSet = TRUE;
                  }

                  if( resNum )
                  {
                      sprintf( Msg, "You are %s (%d)", resArrX.pMembers[0].KeyValue, (int)resArrX.pMembers[0].FarAttained.N );
                      printf("You are %s (%d)", resArrX.pMembers[0].KeyValue, (int)resArrX.pMembers[0].FarAttained.N );
                      char user[] = "";
                      memcpy(user, resArrX.pMembers[0].KeyValue, 4);
                      user[4] = '\0'; 
                      Find = TRUE;
                      return user;
                  }
                  else{
                    printf("No Match Found\n");
                  }


                  if( StopAnyOperation )
                  {
                        strcat( Msg, "Canceled by user" );
                        ErrorSet = TRUE;
                        break;
                  }
                  
                  valNotEnoughOverlap = 0;
                  getRes = FTRGetParam(FTR_PARAM_OVERLAP_NOT_ENOUGH, (FTR_PARAM_VALUE *)&valNotEnoughOverlap);
                  bIsNotEnoughOverlap |= ((getRes == FTR_RETCODE_OK) && (valNotEnoughOverlap != 0));
              }
          }

          closedir( pTemplateDir );

          if( !ErrorSet && !Find )
          {
              if(bIsNotEnoughOverlap)
              {
                  strcat( Msg, "Failed. Not enough overlap" );
              }
              else
              {
                  strcat( Msg, "You not found" );
              }
          }
      }

  }
}

int ReadTemplateFromFile( char* Name, FTR_DATA_PTR pTemplate )
{
    struct stat FileStat;

    if( stat( Name, &FileStat ) < 0 )
    {
        return FALSE;
    }

    pTemplate->pData = malloc( FileStat.st_size );

    if( !pTemplate->pData )
    {
        return FALSE;
    }

    pTemplate->dwSize = FileStat.st_size;
    printf("DWSize: %d\n",pTemplate->dwSize);

    FILE *fp;

    fp = fopen( Name, "rb" );

    if( fp == NULL )
    {
        return FALSE;
    }

    int Result = fread( pTemplate->pData, 1, pTemplate->dwSize, fp ) == pTemplate->dwSize ? TRUE : FALSE;
    printf("pData: %s\n",pTemplate->pData);
    
    fclose( fp );

    return Result;
}



void FreeResource()
{
    FTRTerminate();
}

